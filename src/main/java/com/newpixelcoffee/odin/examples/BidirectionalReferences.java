package com.newpixelcoffee.odin.examples;

import com.newpixelcoffee.odin.Odin;
import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * This example show how Odin manage bidirectional references (parent <> child) and avoid stackoverflow on serialization
 * @author DocVander
 */
public class BidirectionalReferences {

    public static void main(String[] args) {
        // build elements
        Parent parent = new Parent("test");
        parent.add(new ChildNode(parent, "content test with a string"));
        parent.add(new ChildNode(parent, "an another string"));

        // create an Odin instance, with indented output
        Odin odin = new Odin().indentOutput();

        // serialize to a file, there is no StackOverflowException, odin create a reference to parent instead of rewrite parent inside child
        try (OdinWriter writer = odin.writer(new File("bidirectional references.odn"))) {
            writer.write(parent);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // deserialize from file
        try (OdinReader reader = odin.reader(new File("bidirectional references.odn"))) {
            Parent result = reader.read();

            // print first node content
            System.out.println(result.children.get(0).getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // ---------- USED CLASS

    public static class Parent {

        private String name;
        // We can use ArrayList instead of List to avoid Odin to add <array list> type definition
        // This reduce the size of the serialized result
        private final ArrayList<ChildNode> children = new ArrayList<>();

        public Parent(String name) {
            this.name = name;
        }

        public void add(ChildNode node) {
            children.add(node);
        }
    }

    public static class ChildNode {
        private final Parent parent;
        private final String content;

        public ChildNode(Parent parent, String content) {
            this.parent = parent;
            this.content = content;
        }

        public Parent getParent() {
            return parent;
        }

        public String getContent() {
            return content;
        }
    }
}
