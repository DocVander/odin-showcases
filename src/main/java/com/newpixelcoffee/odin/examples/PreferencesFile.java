package com.newpixelcoffee.odin.examples;

import com.newpixelcoffee.odin.Odin;
import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;

import java.io.File;
import java.io.IOException;

/**
 * This example show how Odin can be used to store simple preferences in a file<br/>
 * The format is able to store fields in the stream without any parent node
 * @author DocVander
 */
public class PreferencesFile {

    public static void main(String[] args) {
        // create an Odin instance, with indented output
        Odin odin = new Odin().indentOutput();

        // write some preferences in file
        try (OdinWriter writer = odin.writer(new File("preferences file.odn"))) {
            // write a field xxx = zzz
            writer.writeField("version").withInt(1);

            // we can add comment
            writer.writeComment("Auto save options");
            writer.writeField("auto save interval").withLong(6000);
            writer.writeField("auto save message").withString("Saving data...");
        } catch (IOException e) {
            e.printStackTrace();
        }

        // deserialize from file
        try (OdinReader reader = odin.reader(new File("preferences file.odn"))) {
            // if we do not read a field, Odin just skip the line
            reader.readField("auto save interval").listenLong(interval -> System.out.println("Auto save interval : " + interval));
            reader.readField("auto save message").listenString(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
