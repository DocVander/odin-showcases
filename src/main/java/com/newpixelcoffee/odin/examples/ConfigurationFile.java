package com.newpixelcoffee.odin.examples;

import com.newpixelcoffee.odin.Odin;
import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.elements.OdinNode;

import java.io.File;
import java.io.IOException;

/**
 * Unlike {@link PreferencesFile}, this example write fields in a parent node and allow to search for a value after the read complete
 * @author DocVander
 */
public class ConfigurationFile {

    public static void main(String[] args) {
        // create an Odin instance, with indented output
        Odin odin = new Odin().indentOutput();

        // create the node
        OdinNode node = new OdinNode();
        node.add("version", 1);
        node.add("auto save interval", 6000).getExtra().addComment("Auto save options");
        node.add("auto save message", "Saving data...");

        // we also add some users
        node.addArray("users").run(array -> {
            array.addNode().run(user1 -> {
                user1.add("name", "Administrator");
                user1.add("group", "administrators");
            });
            array.addNode().run(user2 -> {
                user2.add("name", "Guest");
                user2.add("group", "default");
            });
        });

        // write some preferences in file
        try (OdinWriter writer = odin.writer(new File("configuration file.odn"))) {
            writer.writeNode(node);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // deserialize from file
        try (OdinReader reader = odin.reader(new File("configuration file.odn"))) {
            OdinNode result = reader.readNode();

            System.out.println("Auto save interval: " + result.getValue("auto save interval"));
            System.out.println(result.<String>getValue("auto save message"));

            System.out.println("Users: " + node.get("users").asArray().size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
